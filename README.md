# ametek7270

European Spallation Source ERIC Site-specific EPICS module: ametek7270

Additonal information:
* [Documentation](https://confluence.esss.lu.se/display/IS/Integration+by+ICS)
* [Release notes](RELEASE.md)
* [Requirements](https://gitlab.esss.lu.se/nice/staging-recipes/ametek7270-recipe/-/blob/master/recipe/meta.yaml#L18)
* [Building and Testing](https://confluence.esss.lu.se/display/IS/1.+Development+locally+with+e3+and+Conda#id-1.Developmentlocallywithe3andConda-BuildanEPICSmoduleandtestitlocally)
